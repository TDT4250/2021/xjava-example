/**
 * generated by Xtext 2.25.0
 */
package tdt4250.xjava;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.xjava.ParameterDeclaration#getTypeRef <em>Type Ref</em>}</li>
 *   <li>{@link tdt4250.xjava.ParameterDeclaration#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see tdt4250.xjava.XjavaPackage#getParameterDeclaration()
 * @model
 * @generated
 */
public interface ParameterDeclaration extends EObject
{
  /**
   * Returns the value of the '<em><b>Type Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type Ref</em>' containment reference.
   * @see #setTypeRef(TypeReference)
   * @see tdt4250.xjava.XjavaPackage#getParameterDeclaration_TypeRef()
   * @model containment="true"
   * @generated
   */
  TypeReference getTypeRef();

  /**
   * Sets the value of the '{@link tdt4250.xjava.ParameterDeclaration#getTypeRef <em>Type Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type Ref</em>' containment reference.
   * @see #getTypeRef()
   * @generated
   */
  void setTypeRef(TypeReference value);

  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see tdt4250.xjava.XjavaPackage#getParameterDeclaration_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link tdt4250.xjava.ParameterDeclaration#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

} // ParameterDeclaration
